//
//  ViewController.swift
//  PokedexLauria
//
//  Created by COTEMIG on 21/02/22.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var titleLabel: UILabel!

    @IBOutlet weak var loginText: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        print("viewDidLoad - A tela foi carregada")
        
        let tapGesture = UITapGestureRecognizer(target: self,action: #selector(clickNoLabel))
        titleLabel.isUserInteractionEnabled = true
        titleLabel.addGestureRecognizer(tapGesture)
    }
    
    @objc func clickNoLabel(){
        print("clicou no label")
    }

    
    @IBAction func clickPokedex(_ sender: Any) {
        titleLabel.text = "Clicou em POKEDEX"
        titleLabel.textColor = .red
    }
    
    @IBAction func clickMovimento(_ sender: Any) {
        titleLabel.text = "Clicou em MOVIMENTOS"
        titleLabel.textColor = .orange
    }
    
    @IBAction func clickHabilidades(_ sender: Any) {
        titleLabel.text = "Clicou em HABILIDADES"
        titleLabel.textColor = .blue
    }
    
    @IBAction func clickTypeCharts(_ sender: Any) {
        titleLabel.text = "Clicou em TYPE CHARTS"
        titleLabel.textColor = .brown
    }
    
    @IBAction func clickLocalizacao(_ sender: Any) {
        titleLabel.text = "Clicou em LOCALIZACAO"
        titleLabel.textColor = .purple
    }
    
    @IBAction func clickItens(_ sender: Any) {
        titleLabel.text = "Clicou em ITENS"
        titleLabel.textColor = .yellow
    }
    

    override func viewWillAppear(_ animated: Bool) {
        print("viewWillApear - A tela será exbida")
    }
    
    override func viewDidAppear(_ animated: Bool) {
        print("viewDidApear -A tela foi exibida")
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        print("viewWillDisappear -A tela irá desaparecer")
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        print("viewDidDisappear -A tela desapareceu")
    }
    
    @IBAction func onClickLogin(_ sender: Any) {
        
    }
}

